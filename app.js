document.addEventListener("DOMContentLoaded", ()=> {

let startButton = document.querySelector(".start-btn");
let charcterName = document.querySelector(".charcter-name");
let characterSelectAudio = document.querySelector(".character-select-audio");
let characterHoverAudio = document.querySelector(".character-audio");
let titleAudio = document.querySelector(".title-audio");
let backHomeButton = document.querySelector(".back-home-btn");
let caracterContainer = document.querySelector(".character-select-container");
var name = ""
titleAudio.play()
titleAudio.loop = true;

let content = [
    {name:"ryu",id:0,img:"images/ryu.jpg"},
    {name:"ken",id:1,img:"images/ken.jpeg"},
    {name:"honda",id:2,img:"images/honda.jpg"},
    {name:"zangief",id:3,img:"images/zangief.jpg"},
    {name:"blanka",id:4,img:"images/blanka.jpg"}
]

startButton.addEventListener("click", function () {
    titleAudio.pause()
    titleAudio.currentTime = 0;
    characterSelectAudio.play();
    characterSelectAudio.loop = true;
});
backHomeButton.addEventListener("click", function () {
    characterSelectAudio.pause();
    characterSelectAudio.currentTime = 0;
});

// characterBox.forEach(character => {
//     character.addEventListener("mouseover",function () {
//         characterHoverAudio.currentTime = 0;
//         characterHoverAudio.play()
//     })
// });

for (let i = 0; i < content.length; i++) {
    const divElement = document.createElement('div')
    divElement.setAttribute('class','character')
    caracterContainer.appendChild(divElement)
     const buttonElement = document.createElement('button');
     buttonElement.setAttribute('class','character-button');
     buttonElement.setAttribute('name',content[i].name)
     divElement.appendChild(buttonElement)
     const imgElement = document.createElement('img');
     imgElement.setAttribute('class','character-image');
     imgElement.setAttribute("src",content[i].img)
    buttonElement.appendChild(imgElement);
    characterSound()
}
function characterSound() {
    let characterBox = document.querySelectorAll(".character-button");
    characterBox.forEach(character => {
        character.addEventListener("mouseover",function (e) {
            characterHoverAudio.currentTime = 0;
            characterHoverAudio.play()
            charcterName.innerText = e.currentTarget.name
            console.log(e.currentTarget);
        })
    });
};
})
